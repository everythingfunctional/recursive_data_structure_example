program main
    use fibonacci_m, only: generate_fibonacci_calculator
    implicit none

    integer :: nth_number
    
    print *, "Which fibonacci number would you like?"
    read(*,*) nth_number

    associate(calculator => generate_fibonacci_calculator(nth_number))
        print *, calculator%calculate()
    end associate
end program
