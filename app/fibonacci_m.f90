module fibonacci_m
    implicit none

    type, abstract :: fibonacci_abstract_t
    contains
        procedure(calculate_i), public, deferred :: calculate
    end type

    abstract interface
        function calculate_i(self) result(fib_number)
            import :: fibonacci_abstract_t
            implicit none
            class(fibonacci_abstract_t), intent(in) :: self
            integer :: fib_number
        end function
    end interface

    type, extends(fibonacci_abstract_t) :: fibonacci_base_t
    contains
        procedure, public :: calculate => base_calculate
    end type

    type, extends(fibonacci_abstract_t) :: fibonacci_recursive_t
        class(fibonacci_abstract_t), allocatable :: n_minus_1, n_minus_2
    contains
        procedure, public :: calculate => recursive_calculate
    end type
contains
    function base_calculate(self) result(fib_number)
        class(fibonacci_base_t), intent(in) :: self
        integer :: fib_number

        associate(unused => self)
        end associate

        fib_number = 1
    end function

    recursive function recursive_calculate(self) result(fib_number)
        class(fibonacci_recursive_t), intent(in) :: self
        integer :: fib_number

        fib_number = self%n_minus_1%calculate() + self%n_minus_2%calculate()
    end function
    
    recursive function generate_fibonacci_calculator(nth_number) result(calculator)
        integer, intent(in) :: nth_number
        class(fibonacci_abstract_t), allocatable :: calculator

        if (nth_number <= 2) then
            calculator = fibonacci_base_t()
        else
            calculator = fibonacci_recursive_t( &
                    generate_fibonacci_calculator(nth_number - 1), &
                    generate_fibonacci_calculator(nth_number - 2))
        end if
    end function
end module
